<?php

namespace App\Tests\Application;

use App\Application\MemberService;
use App\Dto\Request\ProjectToRequest;
use App\Entity\Member;
use App\Entity\Project;
use App\Repository\MemberRepository;
use App\Repository\ProjectRepository;
use PHPUnit\Framework\TestCase;

class MemberServiceTest extends TestCase
{

    const PROJECT_NAME = 'test';
    const DESCRIPTION_NAME = 'test';
    const FIRST_NAME = 'mario';
    const LAST_NAME = 'rossi';

    private $projectRepositoryMock;
    private $memberRepositoryMock;

    public function setUp()
    {
        $this->memberRepositoryMock = $this->createMock(MemberRepository::class);
        $this->projectRepositoryMock = $this->createMock(ProjectRepository::class);
    }

    public function testAddProjectReturnAMember()
    {

        $project = Project::fromArray(
            [
                'name' => self::PROJECT_NAME,
                'description' => self::DESCRIPTION_NAME
            ]
        );

        $this->projectRepositoryMock->expects($this->any())
            ->method('findById')
            ->willReturn($project);

        $member = Member::fromArray(
            [
                'firstName' => self::FIRST_NAME,
                'lastName' => self::LAST_NAME
            ]
        );

        $this->memberRepositoryMock->expects($this->any())
            ->method('findById')
            ->willReturn($member);

        $projectToRequest = new ProjectToRequest();
        $projectToRequest->setProjectId(1);

        $memberService = new MemberService(
            $this->memberRepositoryMock,
            $this->projectRepositoryMock
        );
        $output = $memberService->addProject(1, $projectToRequest);

        $this->assertInstanceOf(Member::class, $output);

    }
}
