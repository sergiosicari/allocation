<?php

namespace App\Tests\Application;


use App\Application\ProjectService;
use App\Dto\Request\ProjectCreateRequest;
use App\Entity\Project;
use App\Repository\ProjectRepository;
use PHPUnit\Framework\TestCase;

class ProjectServiceTest extends TestCase
{
    public function testCreateReturnProject()
    {

        $projectRepository = $this->createMock(ProjectRepository::class);

        $projectCreateRequest = new ProjectCreateRequest();
        $projectCreateRequest->setName('test');
        $projectCreateRequest->setDescription('test');

        $projectService = new ProjectService($projectRepository);
        $output = $projectService->create($projectCreateRequest);

        $this->assertInstanceOf(Project::class, $output);

    }
}
