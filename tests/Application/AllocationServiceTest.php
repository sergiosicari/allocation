<?php

namespace App\Tests\Application;

use App\Application\AllocationService;
use App\Dto\Request\AllocationCreateRequest;
use App\Entity\Allocation;
use App\Entity\Member;
use App\Entity\Project;
use App\Repository\AllocationRepository;
use App\Repository\MemberRepository;
use App\Repository\ProjectRepository;
use PHPUnit\Framework\TestCase;

class AllocationServiceTest extends TestCase
{

    const MEMBER_ID = 1;
    const PROJECT_ID = 1;
    const DATE = '2020-05-26';
    const HOURS = 1;
    const PROJECT_NAME = 'test';
    const DESCRIPTION_NAME = 'test';
    const FIRST_NAME = 'mario';
    const LAST_NAME = 'rossi';

    private $projectRepositoryMock;
    private $memberRepositoryMock;
    private $allocationRepositoryMock;

    public function setUp()
    {
        $this->projectRepositoryMock = $this->createMock(ProjectRepository::class);
        $this->memberRepositoryMock = $this->createMock(MemberRepository::class);
        $this->allocationRepositoryMock = $this->createMock(AllocationRepository::class);
    }

    public function testCreateAllocationReturnAllocation()
    {
        $project = Project::fromArray(
            [
                'name' => self::PROJECT_NAME,
                'description' => self::DESCRIPTION_NAME
            ]
        );
        $this->projectRepositoryMock->expects($this->any())
            ->method('findById')
            ->willReturn($project);

        $member = Member::fromArray(
            [
                'firstName' => self::FIRST_NAME,
                'lastName' => self::LAST_NAME
            ]
        );
        $this->memberRepositoryMock->expects($this->any())
            ->method('findById')
            ->willReturn($member);

        $allocationService = new AllocationService(
            $this->allocationRepositoryMock,
            $this->projectRepositoryMock,
            $this->memberRepositoryMock
        );

        $allocationCreateRequest = new AllocationCreateRequest();
        $allocationCreateRequest->setProjectId(self::PROJECT_ID);
        $allocationCreateRequest->setMemberId(self::MEMBER_ID);
        $allocationCreateRequest->setDate(self::DATE);
        $allocationCreateRequest->setHours(self::HOURS);

        $allocation = $allocationService->createAllocation($allocationCreateRequest);
        $this->assertInstanceOf(Allocation::class, $allocation);

    }

    public function testGetAllocation()
    {

        $allocationService = new AllocationService(
            $this->allocationRepositoryMock,
            $this->projectRepositoryMock,
            $this->memberRepositoryMock
        );

        $allocations = $allocationService->getAllocationByMember(1);
        $this->assertIsArray($allocations);
    }
}
