# Repo And Demo

This is a demo about a simple single page application.

 BACKEND (Symfony)

- [Backend Repository (GitLab)](https://gitlab.com/sergiosicari/allocation)

- [Backend Demo (Nelmio API Doc on Heroku)](https://desolate-dusk-20743.herokuapp.com/api/doc)

FRONTEND (Vue.js)

- [Frontend Repo (GitLab)](https://gitlab.com/sergiosicari/allocation-frontend)

- [Frontend Demo (GitLab Pages)](https://sergiosicari.gitlab.io/allocation-frontend)

# Requires

* PHP 7.4

# Run with docker

Create an .env.local file from .env

```bash
cp .env .env.local
```

On `.env.local` change DATABASE_URL configuration string 

```bash
DATABASE_URL=mysql://root:root@mysql:3306/allocation_dev?serverVersion=5.7
```

Now start docker-compose

```bash
docker-compose up -d
```

or using make command

```bash
make docker_start
```

P.S.: Wait composer install task finish before test api url

Install db

```bash
# find container name
docker container ls -a
# generate db using php-fpm container
docker exec -ti <container_name> php bin/console doctrine:database:create --env=dev
# install migration using php-fpm container
docker exec -ti <container_name> php bin/console doctrine:migration:migrate --env=dev
```

# Test Api

If you run application with docker go to browser and visit link:

[http://localhost/api/doc](http://localhost/api/doc)

# Install via composer

Create a local .env configuration file and set mysql string connection

```bash
cp .env .env.local
```

```bash
make install_all
```

Also you can start built-in Symfony server:

```
symfony server:start -d
```

visit this local link:

[http://127.0.0.1:8000/api/doc](http://127.0.0.1/api/doc)

P.S.: in this case change local api url in SPA application configuration or force built in server to set 80 port in this way:

```
sudo symfony server:start --port=80 --no-tls -d
```

# Unit Test

```bash
make test
```
