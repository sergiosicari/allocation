<?php

namespace App\DataFixtures;

use App\Entity\Member;
use App\Entity\Project;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    const PROJECTS = [
        [
            'name' => 'nutribees',
            'description' => '',
        ],
        [
            'name' => 'barberinos',
            'description' => '',
        ],
    ];

    const members = [
        [
            'firstName' => 'marco',
            'lastName' => 'rossi',
        ],
        [
            'firstName' => 'mauro',
            'lastName' => 'italia',
        ],
    ];

    public function load(ObjectManager $manager)
    {
        foreach (self::PROJECTS as $item) {
            $project = Project::fromArray($item);
            $manager->persist($project);
        }

        foreach (self::members as $item) {
            $member = Member::fromArray($item);
            $manager->persist($member);
        }

        $manager->flush();
    }
}
