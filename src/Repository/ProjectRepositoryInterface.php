<?php

namespace App\Repository;

use App\Entity\Project;

interface ProjectRepositoryInterface
{
    public function findById(int $id);

    public function getResultsByFilters(array $filters = [], array $orderBy = null, $limit = null, $offset = null);

    public function save(Project $items);
}
