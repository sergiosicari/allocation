<?php

namespace App\Repository;

use App\Entity\Allocation;

interface AllocationRepositoryInterface
{
    public function getResultsByFilters(array $filters = [], array $orderBy = null, $limit = null, $offset = null): array;
    public function save(Allocation $allocation): void;
}
