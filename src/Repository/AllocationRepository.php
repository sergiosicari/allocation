<?php

namespace App\Repository;

use App\Entity\Allocation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Allocation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Allocation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Allocation[]    findAll()
 * @method Allocation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AllocationRepository extends ServiceEntityRepository implements AllocationRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Allocation::class);
    }

    public function getResultsByFilters(array $filters = [], array $orderBy = null, $limit = null, $offset = null): array
    {
        $allocationRepository = $this->getEntityManager()->getRepository(Allocation::class);

        return $allocationRepository->findBy($filters, $orderBy, $limit, $offset);
    }

    public function save(Allocation $allocation): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($allocation);
        $entityManager->flush();
    }
}
