<?php

namespace App\Repository;

use App\Entity\Member;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Member|null find($id, $lockMode = null, $lockVersion = null)
 * @method Member|null findOneBy(array $criteria, array $orderBy = null)
 * @method Member[]    findAll()
 * @method Member[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MemberRepository extends ServiceEntityRepository implements MemberRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Member::class);
    }

    public function findById(int $id): ?Member
    {
        return $this->find($id);
    }

    public function getResultsByFilters(array $filters = [], array $orderBy = null, $limit = null, $offset = null)
    {
        $memberRepository = $this->getEntityManager()->getRepository(Member::class);

        return $memberRepository->findBy($filters, $orderBy, $limit, $offset);
    }

    public function save(Member $member): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($member);
        $entityManager->flush();
    }
}
