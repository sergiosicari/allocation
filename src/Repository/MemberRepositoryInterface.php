<?php

namespace App\Repository;

use App\Entity\Member;

interface MemberRepositoryInterface
{
    public function findById(int $id);

    public function getResultsByFilters(array $filters = [], array $orderBy = null, $limit = null, $offset = null);

    public function save(Member $member);
}
