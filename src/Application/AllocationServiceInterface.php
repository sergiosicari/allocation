<?php

namespace App\Application;

use App\Dto\Request\AllocationCreateRequest;
use App\Entity\Allocation;

interface AllocationServiceInterface
{   public function getAllocationByMember(int $id): array;
    public function createAllocation(AllocationCreateRequest $allocationCreateRequest): Allocation;
}
