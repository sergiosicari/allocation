<?php

namespace App\Application;

use App\Dto\Request\ProjectToRequest;
use App\Entity\Member;

interface MemberServiceInterface
{
    public function get(array $filters = []): array;

    public function addProject(int $id, ProjectToRequest $projectToRequest): Member;

    public function removeProject(int $id, ProjectToRequest $projectToRequest): void;
}
