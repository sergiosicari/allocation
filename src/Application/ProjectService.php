<?php

namespace App\Application;

use App\Dto\Request\ProjectCreateRequest;
use App\Entity\Project;
use App\Repository\ProjectRepositoryInterface;

class ProjectService implements ProjectServiceInterface
{
    private $projectRepository;

    public function __construct(ProjectRepositoryInterface $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }

    public function get(array $filters = []): array
    {
        $projects = $this->projectRepository->getResultsByFilters($filters);

        return $projects;
    }

    public function create(
        ProjectCreateRequest $request
    ): Project {
        $project = Project::fromDto($request);
        $this->projectRepository->save($project);

        return $project;
    }
}
