<?php

namespace App\Application;

use App\Dto\Request\ProjectCreateRequest;
use App\Entity\Project;

interface ProjectServiceInterface
{
    public function get(array $filters = []): array;

    public function create(ProjectCreateRequest $projectCreateRequest): Project;
}
