<?php

namespace App\Application;

use App\Dto\Request\ProjectToRequest;
use App\Entity\Member;
use App\Repository\MemberRepositoryInterface;
use App\Repository\ProjectRepositoryInterface;

class MemberService implements MemberServiceInterface
{
    private $memberRepository;
    private $projectRepository;

    public function __construct(
        MemberRepositoryInterface $memberRepository,
        ProjectRepositoryInterface $projectRepository)
    {
        $this->memberRepository = $memberRepository;
        $this->projectRepository = $projectRepository;
    }

    public function get(array $filters = []): array
    {
        $members = $this->memberRepository->getResultsByFilters($filters);

        return $members;
    }

    public function addProject(int $id, ProjectToRequest $projectToRequest): Member
    {
        $project = $this->projectRepository->findById($projectToRequest->getProjectId());
        if (empty($project)) {
            throw new \Exception('project not found');
        }

        $member = $this->memberRepository->findById($id);
        if (empty($member)) {
            throw new \Exception('member not found');
        }

        $member->addProject($project);

        $this->memberRepository->save($member);

        return $member;
    }

    public function removeProject(int $id, ProjectToRequest $projectToRequest): void
    {
        $project = $this->projectRepository->findById($projectToRequest->getProjectId());
        if (empty($project)) {
            throw new \Exception('project not found');
        }

        $member = $this->memberRepository->findById($id);
        if (empty($member)) {
            throw new \Exception('member not found');
        }

        $member->removeProject($project);

        $this->memberRepository->save($member);
    }
}
