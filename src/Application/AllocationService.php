<?php

namespace App\Application;

use App\Dto\Request\AllocationCreateRequest;
use App\Entity\Allocation;
use App\Repository\AllocationRepositoryInterface;
use App\Repository\MemberRepositoryInterface;
use App\Repository\ProjectRepositoryInterface;
use Doctrine\Common\Collections\ArrayCollection;

class AllocationService implements AllocationServiceInterface
{
    private $allocationRepository;
    private $projectRepository;
    private $memberRepository;

    public function __construct(
        AllocationRepositoryInterface $allocationRepository,
        ProjectRepositoryInterface $projectRepository,
        MemberRepositoryInterface $memberRepository
    ) {
        $this->allocationRepository = $allocationRepository;
        $this->projectRepository = $projectRepository;
        $this->memberRepository = $memberRepository;
    }

    public function createAllocation(AllocationCreateRequest $allocationCreateRequest): Allocation
    {
        $project = $this->projectRepository->findById($allocationCreateRequest->getProjectId());
        if (empty($project)) {
            throw new \Exception('project not found');
        }

        $member = $this->memberRepository->findById($allocationCreateRequest->getMemberId());
        if (empty($member)) {
            throw new \Exception('member not found');
        }

        $allocation = Allocation::fromData(
            $project,
            $member,
            \DateTime::createFromFormat('Y-m-d', $allocationCreateRequest->getDate()),
            $allocationCreateRequest->getHours()
        );
        $this->allocationRepository->save($allocation);

        return $allocation;
    }

    public function getAllocationByMember(int $id): array
    {
        $allocations = $this->allocationRepository->getResultsByFilters(['member' => $id]);
        return $allocations;
    }
}
