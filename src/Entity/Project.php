<?php

namespace App\Entity;

use App\Dto\Request\ProjectCreateRequest;
use App\Repository\ProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProjectRepository::class)
 */
class Project
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="Member", mappedBy="projects")
     */
    private $member;

    private function __construct(
        string $name,
        string $description = null)
    {
        $this->name = $name;
        $this->description = $description;
        $this->member = new ArrayCollection();
    }

    public static function fromDto(ProjectCreateRequest $projectCreateRequest): self
    {
        return new self(
            $projectCreateRequest->getName(),
            $projectCreateRequest->getDescription()
        );
    }

    public static function fromArray(array $project): self
    {
        return new self(
            $project['name'],
            $project['description']
        );
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Member[]
     */
    public function getMember(): Collection
    {
        return $this->member;
    }

    public function addMember(Member $member): self
    {
        if (!$this->member->contains($member)) {
            $this->member[] = $member;
            $member->addProject($this);
        }

        return $this;
    }

    public function removeMember(Member $member): self
    {
        if ($this->member->contains($member)) {
            $this->member->removeElement($member);
            $member->removeProject($this);
        }

        return $this;
    }
}
