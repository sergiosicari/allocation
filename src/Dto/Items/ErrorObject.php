<?php

namespace App\Dto\Items;

use Symfony\Component\Validator\Constraints as Assert;

class ErrorObject implements \JsonSerializable
{
    /**
     * @var string
     */
    private $detail;

    /**
     * @var string|null
     */
    private $title;

    /**
     * @var string|null
     */
    private $source;

    public function __construct(string $detail)
    {
        $this->detail = $detail;
    }

    public function getDetail(): string
    {
        return $this->detail;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @return ErrorObject
     */
    public function setSource(string $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return ErrorObject
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
