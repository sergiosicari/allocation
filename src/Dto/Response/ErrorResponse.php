<?php

namespace App\Dto\Response;

use App\Dto\Items\ErrorObject;

/**
 * Class ErrorResponse.
 *
 * This class represents the error response according
 * to JSON API standard https://jsonapi.org/format/#error-objects
 *
 * All JSON errors (including validation) must be
 * encapsulated in an instance of this class.
 *
 * You can make helper/builder/assembler/utility class to
 * map your error or exception to an instance of this class.
 */
class ErrorResponse
{
    /**
     * @var ErrorObject[]
     */
    private $errors;

    public function __construct(array $errors = [])
    {
        $this->errors = $errors;
    }

    public function add(ErrorObject $error)
    {
        $this->errors[] = $error;
    }

    /**
     * @return ErrorObject[]
     */
    public function all(): array
    {
        return $this->errors;
    }
}
