<?php

namespace App\Dto\Request;

use Symfony\Component\Validator\Constraints as Assert;

class ProjectCreateRequest
{
    /**
     * @var string
     * @Assert\NotNull(message="name is required xxx.")
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min=2,
     *     max=255,
     *     minMessage="name is too short. It should have 2 characters or more.",
     *     maxMessage="name is too long. It should have 50 characters or less."
     * )
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }
}
