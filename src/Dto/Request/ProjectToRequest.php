<?php

namespace App\Dto\Request;

use Symfony\Component\Validator\Constraints as Assert;

class ProjectToRequest
{
    /**
     * @var int
     * @Assert\Type("int")
     * @Assert\NotNull(message="project id is required.")
     */
    private $projectId;

    public function setProjectId(int $projectId): void
    {
        $this->projectId = $projectId;
    }

    public function getProjectId(): int
    {
        return $this->projectId;
    }
}
