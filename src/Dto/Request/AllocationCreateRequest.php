<?php

namespace App\Dto\Request;

use App\Entity\Member;
use App\Entity\Project;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Date;

class AllocationCreateRequest
{
    /**
     * @var int
     * @Assert\Type("int")
     * @Assert\NotNull(message="project id is required.")
     */
    private $projectId;
    /**
     * @var int
     * @Assert\Type("int")
     * @Assert\NotNull(message="member id is required.")
     */
    private $memberId;
    /**
     * @var string A "Y-m-d" formatted value
     * @Assert\DateTime("Y-m-d")
     * @Assert\NotNull(message="date is required.")
     */
    private $date;
    /**
     * @var int
     * @Assert\Type("int")
     * @Assert\Positive()
     * @Assert\NotNull(message="hours is required.")
     */
    private $hours;

    public function getProjectId(): int
    {
        return $this->projectId;
    }

    public function setProjectId(int $projectId): void
    {
        $this->projectId = $projectId;
    }

    public function getMemberId(): int
    {
        return $this->memberId;
    }

    public function setMemberId(int $memberId): void
    {
        $this->memberId = $memberId;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    public function getHours(): int
    {
        return $this->hours;
    }

    public function setHours(int $hours): void
    {
        $this->hours = $hours;
    }
}
