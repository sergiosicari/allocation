<?php

namespace App\Controller;

use App\Application\AllocationServiceInterface;
use App\Controller\Traits\ErrorResponseTrait;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AllocationCreateController extends AbstractController
{
    use ErrorResponseTrait;

    private $allocationService;
    private $serializer;
    private $validator;

    public function __construct(
        AllocationServiceInterface $allocationService,
        SerializerInterface $serializer,
        ValidatorInterface $validator
    ) {
        $this->allocationService = $allocationService;
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    /**
     * @Route("/api/allocations/", name="create_member_allocations", methods={"POST"})
     *
     * @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=App\Dto\Request\AllocationCreateRequest::class)
     *     )
     * )
     *
     * @SWG\Response(
     *     response=JsonResponse::HTTP_NO_CONTENT,
     *     description="Success"
     * )
     *
     * @SWG\Tag(name="Member")
     *
     * @throws \Exception
     */
    public function __invoke(Request $request): JsonResponse
    {

        $dtoRequest = $this->serializer->deserialize(
            $request->getContent(),
            'App\Dto\Request\AllocationCreateRequest[]',
            'json'
        );

        $errors = $this->validator->validate($dtoRequest);

        if (count($errors) > 0) {
            $errorResponse = $this->constraintViolationView($errors);

            return $errorResponse;
        }

        foreach ($dtoRequest as $item) {
            $this->allocationService->createAllocation($item);
        }

        return JsonResponse::create([], JsonResponse::HTTP_NO_CONTENT);
    }
}
