<?php

namespace App\Controller;

use App\Application\MemberServiceInterface;
use App\Controller\Traits\ErrorResponseTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;

class MemberListController extends AbstractController
{
    use ErrorResponseTrait;

    private $memberService;
    private $serializer;

    public function __construct(
        MemberServiceInterface $memberService,
        SerializerInterface $serializer
    ) {
        $this->memberService = $memberService;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/api/members/", name="get_members", methods={"GET"})
     * @SWG\Response(
     *     response="200",
     *     description="get members list",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=App\Entity\Member::class)
     *     )
     * )
     * @SWG\Tag(name="Management")
     */
    public function __invoke(): JsonResponse
    {

        $members = $this->memberService->get();

        $jsonResponse = $this->serializer->serialize(
            $members,
            'json',
            [
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]
        );

        return new JsonResponse($jsonResponse, JsonResponse::HTTP_OK, [], true);
    }
}
