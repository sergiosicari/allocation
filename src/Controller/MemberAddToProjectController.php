<?php

namespace App\Controller;

use App\Application\MemberServiceInterface;
use App\Controller\Traits\ErrorResponseTrait;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class MemberAddToProjectController extends AbstractController
{
    use ErrorResponseTrait;

    private $memberService;
    private $serializer;
    private $validator;

    public function __construct(
        MemberServiceInterface $memberService,
        SerializerInterface $serializer,
        ValidatorInterface $validator
    ) {
        $this->memberService = $memberService;
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    /**
     * @Route("/api/members/{id}/projects/", name="add_project_to_member", methods={"POST"})
     *
     * @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=App\Dto\Request\ProjectToRequest::class)
     *     )
     * )
     *
     * @SWG\Response(
     *     response=JsonResponse::HTTP_NO_CONTENT,
     *     description="Success",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="success", type="string")
     *     )
     * )
     *
     * @SWG\Tag(name="Management")
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function __invoke(int $id, Request $request)
    {
        $dtoRequests = $this->serializer->deserialize(
            $request->getContent(),
            'App\Dto\Request\ProjectToRequest[]',
            'json'
        );

        $errors = $this->validator->validate($dtoRequests);

        if (count($errors) > 0) {
            $errorResponse = $this->constraintViolationView($errors);

            return $errorResponse;
        }

        foreach ($dtoRequests as $item) {
            $this->memberService->addProject($id, $item);
        }

        return JsonResponse::create([], JsonResponse::HTTP_NO_CONTENT);
    }
}
