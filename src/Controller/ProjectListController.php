<?php

namespace App\Controller;

use App\Application\ProjectServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;

use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;

class ProjectListController extends AbstractController
{
    private $projectService;
    private $serializer;

    public function __construct(
        ProjectServiceInterface $projectService,
        SerializerInterface $serializer
    ) {
        $this->projectService = $projectService;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/api/projects/", name="get_projects", methods={"GET"})
     * @SWG\Response(
     *     response="200",
     *     description="get projects list",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=App\Entity\Project::class)
     *     )
     * )
     * @SWG\Tag(name="Management")
     */
    public function __invoke(): JsonResponse
    {

        $projects = $this->projectService->get();

        $jsonResponse = $this->serializer->serialize(
            $projects,
            'json',
            [
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]
        );

        return new JsonResponse($jsonResponse, JsonResponse::HTTP_OK, [], true);
    }
}
