<?php

namespace App\Controller;

use App\Application\ProjectServiceInterface;
use App\Controller\Traits\ErrorResponseTrait;
use App\Dto\Request\ProjectCreateRequest;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProjectCreateController extends AbstractController
{
    use ErrorResponseTrait;

    private $projectService;
    private $serializer;
    private $validator;

    public function __construct(
        ProjectServiceInterface $projectService,
        SerializerInterface $serializer,
        ValidatorInterface $validator
        ) {
        $this->projectService = $projectService;
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    /**
     * @Route("/api/projects/", name="create_project", methods={"POST"})
     *
     * @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     @Nelmio\Model(type=ProjectCreateRequest::class)
     * )
     *
     * @SWG\Response(
     *     response=JsonResponse::HTTP_CREATED,
     *     description="Success",
     *     @SWG\Schema(
     *         type="object",
     *         ref=@Model(type=App\Entity\Project::class)
     *     )
     * )
     *
     * @SWG\Tag(name="Management")
     */
    public function __invoke(Request $request): JsonResponse
    {
        $dtoRequest = $this->serializer->deserialize(
            $request->getContent(),
            ProjectCreateRequest::class,
            'json'
        );

        $errors = $this->validator->validate($dtoRequest);

        if (count($errors) > 0) {
            $errorResponse = $this->constraintViolationView($errors);

            return $errorResponse;
        }

        $project = $this->projectService->create($dtoRequest);

        $jsonResponse = $this->serializer->serialize($project, 'json');

        return new JsonResponse($jsonResponse, JsonResponse::HTTP_CREATED, [], true);
    }
}
