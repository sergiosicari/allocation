<?php

namespace App\Controller\Traits;

use App\Dto\Items\ErrorObject;
use App\Dto\Response\ErrorResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\ConstraintViolationListInterface;

trait ErrorResponseTrait
{
    public function constraintViolationView(ConstraintViolationListInterface $errors): JsonResponse
    {
        $data = new ErrorResponse();

        foreach ($errors as $ve) {
            $e = new ErrorObject($ve->getMessage());
            $e->setSource($ve->getPropertyPath());
            $e->setTitle('Invalid data');
            $data->add($e);
        }

        $response = new JsonResponse();
        $response->setData($data->all());

        return $response;
    }
}
