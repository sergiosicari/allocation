<?php

namespace App\Controller;

use App\Application\MemberServiceInterface;
use App\Controller\Traits\ErrorResponseTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;

class MemberListByIdController extends AbstractController
{
    use ErrorResponseTrait;

    private $memberService;
    private $serializer;

    public function __construct(
        MemberServiceInterface $memberService,
        SerializerInterface $serializer
    ) {
        $this->memberService = $memberService;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/api/members/{id}", name="get_member", methods={"GET"})
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     type="string",
     *     description="resource id"
     * )
     * @SWG\Response(
     *     response="200",
     *     description="get members list",
     *     @SWG\Schema(
     *         type="object",
     *         ref=@Model(type=App\Entity\Member::class)
     *     )
     * )
     * @SWG\Tag(name="Member")
     */
    public function __invoke(Request $request): JsonResponse
    {

        $filters = [];
        if ($request->get('id')) {
            $filters['id'] = (int)$request->get('id');
        }

        $member = $this->memberService->get($filters);


        $jsonResponse = $this->serializer->serialize(
            $member,
            'json',
            [
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]
        );

        return new JsonResponse($jsonResponse, JsonResponse::HTTP_OK, [], true);
    }
}
